package tollroad;

import java.util.HashMap;

/**
 * The main class. Contains a bunch of state and functions that handle
 * most of the busywork the tollroad needs to perform itself.
 */
public class TollRoad
{
    private int moneyMade;
    // HashMap chosen for amortized O(1) access and lookup.
    // Makes findCustomer trivial and quick.
    private HashMap<String,CustomerAccount> customerAccountMap;

    /**
     * Our constructor. Sets our money made to 0, and creates
     * an empty hashmap to store the customers
     */
    public TollRoad()
    {
        moneyMade = 0;
        customerAccountMap = new HashMap<>();
    }

    /**
     * Add a customer to the hashmap.
     * @param acc the account we want to add
     */
    public void addCustomer(CustomerAccount acc)
    {
        customerAccountMap.put(acc.getCustomerVehicle().getRegNo(), acc);
    }

    /**
     * Find and return a customer in the TollRoad's hashmap
     * @param regNum the regestration plate of the account we want
     * @return the account we want.
     * @throws CustomerNotFoundException if customer not in map
     */
    public CustomerAccount findCustomer(String regNum) throws
            CustomerNotFoundException
    {
        CustomerAccount customer = customerAccountMap.get(regNum);
        if(customer == null)
            throw new CustomerNotFoundException();
        else
            return customer;
    }

    /**
     * Charge the customer based on the cost of their trip minus their discount
     * (if applicable)
     * @param regNum The reg plate of the car we want to charge
     * @throws InsufficientAccountBalanceException if the account does not
     * have enough money to make the trip, throw an exception
     * @throws CustomerNotFoundException if we cant find the customer
     * this is thrown.
     */
    public void chargeCustomer(String regNum) throws
            InsufficientAccountBalanceException, CustomerNotFoundException
    {
        CustomerAccount customer;
        customer = findCustomer(regNum);

        if(customer != null)
        {
            int cost = customer.makeTrip();
            customer.addFunds(-cost);
            moneyMade += cost;
        }
    }

    /**
     * Serialize the account
     * @return serialized form of the account
     */
    @Override
    public String toString()
    {
        String retval = "";
        for(String key : customerAccountMap.keySet())
        {
            retval += customerAccountMap.get(key).toString();
            retval += '\n';
            retval += "MoneyMade: " + moneyMade + '\n';
        }
        return retval;
    }

    public int getMoneyMade()
    {
        return moneyMade;
    }
}
