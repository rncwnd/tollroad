package tollroad;

/**
 * Our representation of a customer account.
 * Contains some state and mutators.
 */
public class CustomerAccount implements Comparable<CustomerAccount>
{
    /**
     * Represent all of the different discounts we can apply to an account.
     */
    private enum DiscountType{
        NONE, STAFF, FRIENDS_AND_FAMILY
    }

    // Bunch of state. Represents the person as well as some internal details
    // such as the balance and discount the user is entitled too.
    private String forename;
    private String surname;
    private Vehicle customerVehicle;
    private int accountBalance;
    private DiscountType discount;

    /**
     * Construct an account instance.
     * @param forename Account holder's forename
     * @param surname Account holder's surname
     * @param customerVehicle account holder's vehicle.
     * @param accountBalance money in the account, represented in pennies.
     */
    public CustomerAccount(String forename, String surname,
                           Vehicle customerVehicle, int accountBalance)
    {
        this.forename = forename;
        this.surname = surname;
        this.customerVehicle = customerVehicle;
        this.accountBalance = accountBalance;
        this.discount = DiscountType.NONE;
    }

    /**
     * Calculate the cost of making a trip for this account holder.
     * @return cost of the trip
     */
    public int makeTrip() throws InsufficientAccountBalanceException
    {
        int cost = this.customerVehicle.calculateBasicTripCost();
        if(cost > this.accountBalance)
            throw new InsufficientAccountBalanceException();
        switch(discount){
            case NONE: break;
            case FRIENDS_AND_FAMILY: cost -= (cost / 10); break;
            case STAFF: cost -= (cost/2); break;
        }
        return cost;
    }

    /**
     * Enables the staff discount flag on the account
     */
    void activateStaffDiscount()
    {
        this.discount = DiscountType.STAFF;
    }

    /**
     * Enables the Friends and Family discount on this account
     */
    void activateFriendsAndFamilyDiscount()
    {
        this.discount = DiscountType.FRIENDS_AND_FAMILY;
    }

    /**
     * Disables the discount, in case we need to revoke it
     */
    void deactivateDiscount()
    {
        this.discount = DiscountType.NONE;
    }

    /**
     * Add a value to the account balance, and mutate the state
     * @param amount amount to add
     */
    public void addFunds(int amount)
    {
        this.accountBalance += amount;
    }

    public String getForename(){return this.forename;}
    public String getSurname(){return this.surname;}
    public Vehicle getCustomerVehicle(){return this.customerVehicle;}
    public int getAccountBalance(){return this.accountBalance;}

    /**
     * Implement comparable so we can sort users.
     * @param otherAccount other account we want to compare to
     * @return Sorting order order of the two vehicles, strcmp style
     */
    @Override
    public int compareTo(CustomerAccount otherAccount)
    {
        return this.customerVehicle.getRegNo().compareTo
                (otherAccount.customerVehicle.getRegNo());
    }

    /**
     * Serialize the account
     * @return serialized account information
     */
    @Override
    public String toString()
    {
        return "Forname:" + this.forename + "\tSurname:" + this.surname +
                "\tVehicle:(" +this.customerVehicle.toString() +
                ")\tbalance:" + this.accountBalance + "\tdiscount:" + discount;
    }
}
