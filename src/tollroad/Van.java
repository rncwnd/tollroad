package tollroad;

/**
 * A Van. Inherits from Vehicle
 */
public class Van extends Vehicle
{
    int payload;
    private final String type = "Van"; // Used for serialization

    /**
     * Constructor for the Van
     * @param regNo Reg Plate of the van.
     * @param make Make of the van
     * @param payload amount of weight the van is carrying in kg
     */
    public Van(String regNo, String make, int payload)
    {
        super(regNo, make);
        this.payload = payload;
    }

    /**
     * Overrides the inherited calculate function.
     * Returns an integer based on the amount of cargo being carried.
     * @return the cost of the trip
     */
    @Override
    public int calculateBasicTripCost()
    {
        if(payload <= 600)
            return 500;
        else if(payload <= 800)
            return 750;
        else
            return 1000;
    }

    /**
     * Provide a way of serialising the vehicle data to a string.
     * @return A string representation of the class.
     */
    @Override
    public String toString()
    {
        return "Type:" + this.type + '\t' +
                "regNo:" + getRegNo() + '\t' +
               "Make:" + getMake() + '\t' +
               "Payload:" + getPayload() + '\t'+
               "TripCost:" + calculateBasicTripCost();
    }

    public int getPayload(){return payload;}
}
