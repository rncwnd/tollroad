package tollroad;

public class Truck extends Vehicle
{
    private int numTrailers;
    private final String type = "Truck";

    /**
     * Constructor for the Truck
     * @param regNo Reg Plate of the Truck
     * @param make the make of truck
     * @param numTrailers the number of trailers the truck has
     */
    public Truck(String regNo, String make, int numTrailers)
    {
        super(regNo, make);
        this.numTrailers = numTrailers;
    }

    /**
     * Overrides it's parent. Changes the cost of the trip
     * based on the amount of trailers the truck has
     * @return cost of the trip
     */
    @Override
    public int calculateBasicTripCost()
    {
        if(getNumberOfTrailers() >= 2)
            return 1500;
        else
            return 1250;
    }

    /**
     * ToString for serialization
     * @return Serialized representation of the truck
     */
    @Override
    public String toString()
    {
        return "Type:" + this.type + '\t' +
               "regNo:" + getRegNo() + '\t' +
               "Make:" + getMake() + '\t' +
               "trailerNo:" + getNumberOfTrailers() + '\t'+
               "TripCost:" + calculateBasicTripCost();
    }

    public int getNumberOfTrailers(){return numTrailers;}
}
