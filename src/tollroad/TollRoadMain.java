package tollroad;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Contains our main function, as well as some helper functions
 * to acheive the functionality of the main function.
 */
public class TollRoadMain
{
    // Kinda bad, but having to pass it as an arg is messier.
    private static PrintStream fsout;
    /**
     * Our main function. Creates a tollroad by calling an init func
     * and then simulates the tollroad. If an exception makes it all
     * the way up here, we throw a stack trace at our users.
     * @param args unused, but idiomatic
     */
    public static void main(String[] args)
    {
        try
        {
            fsout = new PrintStream(new FileOutputStream("out.txt"), true);
            TollRoad tollroad = initialiseTollRoadFromFile();
            int moneyMade = simulateFromFile(tollroad);
            fsout.println("\nMade " + moneyMade + " (£" +
                    (double)moneyMade/100 + ") this session");
            fsout.close();
        } catch (Exception e)
        {
            e.printStackTrace();
            fsout.close();
        }
    }

    /**
     * Takes in thje commands file, and then performs the action on
     * the account.
     * @param road the TollRoad we want to simulate
     * @throws IOException if we cant read the transations file.
     */
    private static int simulateFromFile(TollRoad road) throws IOException
    {
        String operations = new String(Files.readAllBytes(Paths.get
                ("transactions.txt")));

        // Split into lines.
        String[] lines = operations.split("\\$");
        // Remove control chars
        for(int i =0; i < lines.length; ++i)
        {
            lines[i] = lines[i].replaceAll("[\\p{Cntrl}]","");
        }

        // Make two lambdas to handle the operations we'll perform.
        addFunds fundsLamb = ((regno, val) ->
            road.findCustomer(regno).addFunds(Integer.parseInt(val)));
        makeTrip tripLamb = (regno ->
            road.chargeCustomer(regno));

        // I miss strtok(3)
        for(int i = 0; i < lines.length; ++i)
        {
            String[] ops = lines[i].split(",");
            // Switch based on "opcode"
            switch(ops[0]){
                case "addFunds":
                    try{
                        fundsLamb.run(ops[1], ops[2]);
                    }
                    catch (CustomerNotFoundException e){
                        fsout.println(ops[1] + ": addFunds failed." +
                                "CustomerAccount does not exist");
                    }
                    break;
                case "makeTrip":
                    try{
                        tripLamb.run(ops[1]);
                    }
                    catch (CustomerNotFoundException e){
                        fsout.println(ops[1] + ": makeTrip failed. " +
                                "CustomerAccount does not exist");
                    }
                    catch (InsufficientAccountBalanceException e){
                        fsout.println(ops[1] + ": makeTrip failed. " +
                                "Insufficient funds");
                    }
                    break;
            }
        }
        return road.getMoneyMade();
    }

    /**
     * Functional interfaces for our operations in the simulate func.
     * Java for some reason requires these for lambdas, and it's annoying.
     */
    interface addFunds{
        void run(String regno, String val) throws CustomerNotFoundException;
    }
    interface makeTrip{
        void run(String regno) throws
                CustomerNotFoundException, InsufficientAccountBalanceException;
    }

    /**
     * Takes in our customerData.txt, tokenises it and creates a new account
     * for each customer in the file. It then adds that to the hashmap
     * of customers
     * @return the mutated TollRoad
     * @throws Exception if we cant read the customerData.txt, or
     *                   if there's no vehicles in the file.
     */
    private static TollRoad initialiseTollRoadFromFile() throws Exception
    {
        TollRoad tollroad = new TollRoad();
        // Read our text file into a string
        String customerDataStr;
        customerDataStr = new String(Files.readAllBytes(Paths.get
                ("customerData.txt")));

        // Split that string based on the '#' delimiter
        String[] customers = customerDataStr.split("#");
        // Remove all the ugly control characters just in case.
        for(int i =0; i < customers.length; ++i)
        {
            customers[i] = customers[i].replaceAll("[\\p{Cntrl}]","");
        }

        // Make an array to hold the accounts temporarily.
        CustomerAccount accounts[] = new CustomerAccount[customers.length];

        // Tokenize each of the lines that we split.
        for(int i = 0; i < customers.length; ++i)
        {
            String[] tokens = customers[i].split(",");

            // Construct a vehicle based on the type that we read.
            Vehicle customerv;
            switch(tokens[0]){
                case "Car" : customerv =
                        new Car(tokens[1], tokens[4],
                            Integer.parseInt(tokens[6])); break;

                case "Truck" : customerv =
                        new Truck(tokens[1], tokens[4],
                            Integer.parseInt(tokens[6])); break;

                case "Van" : customerv =
                        new Van(tokens[1], tokens[4],
                            Integer.parseInt(tokens[6])); break;

                default: fsout.println("No vehicle in tokens!");
                         throw new Exception("Malformed input!");
            }

            // Build a new account for each line, and append that
            // to a temp container
            CustomerAccount tempacc = new CustomerAccount(
                    tokens[2], tokens[3], customerv,
                    Integer.parseInt(tokens[6]));

            // If the customer has a discount type, set that on their
            // account as well.
            switch(tokens[7]) {
                case "NONE" : break;
                case "STAFF" : tempacc.activateStaffDiscount();
                case "FRIENDS_AND_FAMILY" :
                    tempacc.activateFriendsAndFamilyDiscount();
            }

            accounts[i] = tempacc;
        }

        // For every account we read, add them to the hashmap
        for(CustomerAccount acc : accounts)
        {
            tollroad.addCustomer(acc);
        }
        return tollroad;
    }
}
