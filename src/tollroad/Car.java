package tollroad;

/**
 * Car vehicle type.
 */
public class Car extends Vehicle
{
    private int numberOfSeats;
    private final String type = "Car";

    /**
     * Construct a car
     * @param regNo regestration plate of the account holder
     * @param make make of the car
     * @param numberOfSeats number of seats the car has
     */
    public Car(String regNo, String make, int numberOfSeats)
    {
        super(regNo, make);
        this.numberOfSeats = numberOfSeats;
    }

    /**
     * Implement the calculation for trip cost. Cost changes based on
     * amount of seats the car has
     * @return the cost of the trip
     */
    @Override
    public int calculateBasicTripCost()
    {
        if(this.getNumberOfSeats() <= 5)
            return 500;
        else
            return 600;
    }

    /**
     * Serialize out the car
     * @return serialized form of the car
     */
    @Override
    public String toString()
    {

        return "Type:" + this.type + '\t' +
               "regNo:" + getRegNo() + '\t' +
               "Make:" + getMake() + '\t' +
               "NoSeats:" + getNumberOfSeats() + '\t'+
               "TripCost:" + calculateBasicTripCost();
    }

    private int getNumberOfSeats(){return numberOfSeats;}
}
