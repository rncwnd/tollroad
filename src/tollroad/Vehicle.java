package tollroad;

/**
 * An abstract class that all vehicles inherit from.
 * Contains data storage, and a few functions that all vehicles need
 */
public abstract class Vehicle
{
    private String regNo;
    private String make;

    // Better to prevent other people from using it by specifying
    // the default constructor as private.
    private Vehicle(){}

    /**
     * Create a vehicle
     * @param regNo the Registration Plate of the vehicle
     * @param make the Make of the vehicle.
     */
    public Vehicle(String regNo, String make)
    {
        this.regNo = regNo;
        this.make = make;
    }

    public String getRegNo(){return regNo;}

    public String getMake(){return make;}

    /**
     * All children of this class override this to handle their specific
     * requirements for trip costs.
     * @return the cost of the trip.
     */
    abstract public int calculateBasicTripCost();
}
