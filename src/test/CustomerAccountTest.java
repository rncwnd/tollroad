package test;
import tollroad.Car;
import tollroad.CustomerAccount;

class CustomerAccountTest
{
    boolean test()
    {
        String expected = "Forename:John\tSurname:Doe\tAccountBalance:500";
        Car testcar = new Car("TESTPLATE", "TESTMAKE", 4);
        CustomerAccount testacc = new CustomerAccount("John", "Doe",
                testcar, 500);
        System.out.println("Testing CustomerAccount class...");
        String result = "Forename:" + testacc.getForename() + '\t' +
                        "Surname:" + testacc.getSurname() + '\t' +
                        "AccountBalance:" + testacc.getAccountBalance();
        if(expected.equals(result))
            return true;
        else
        {
            System.out.println("\tExpected:\n\t\t" + expected);
            System.out.println("\tGot:\n\t\t" + result);
            return false;
        }
    }
}
