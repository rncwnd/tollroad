package test;

import tollroad.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

class TollRoadMainTest
{
    // Just make it retain state instead of refactoring the whole harness
    // to handle this edge case. Thanks
    private static TollRoad tollroad = new TollRoad();

    boolean testInitialise()
    {
        // I miss macros...
        String expectedPreSplit = "Car,CB13IIZ,Cyndi,Banister,Kia,7,1500,STAFF#\n" +
            "Truck,FM66JPE,Blondell,Boaz,Ford,1,700,FRIENDS_AND_FAMILY#\n" +
            "Van,YJ55XPM,Jessie,Ruhland,Vauxhall,450,1200,NONE#";
        String[] expectedPostSplit =
               {"Car,CB13IIZ,Cyndi,Banister,Kia,7,1500,STAFF",
               "Truck,FM66JPE,Blondell,Boaz,Ford,1,700,FRIENDS_AND_FAMILY",
               "Van,YJ55XPM,Jessie,Ruhland,Vauxhall,450,1200,NONE"};
        String[][] expectedPostTokenize =
                {{"Car","CB13IIZ","Cyndi","Banister","Kia","7","1500","STAFF"},
                 {"Truck","FM66JPE","Blondell","Boaz","Ford","1","700","FRIENDS_AND_FAMILY"},
                 {"Van","YJ55XPM","Jessie","Ruhland","Vauxhall","450","1200","NONE"}};

        String finalExpected =
        "Forname:Jessie\tSurname:Ruhland\tVehicle:(Type:Van\tregNo:YJ55XPM\tMake:Vauxhall\t" +
                "Payload:1200\tTripCost:1000)\tbalance:1200\tdiscount:NONE\n" +
        "Forname:Blondell\tSurname:Boaz\tVehicle:(Type:Truck\tregNo:FM66JPE\tMake:Ford\t" +
                "trailerNo:700\tTripCost:1500)\tbalance:700\tdiscount:NONE\n" +
        "Forname:Cyndi\tSurname:Banister\tVehicle:(Type:Car\tregNo:CB13IIZ\tMake:Kia\t" +
                "NoSeats:1500\tTripCost:600)\tbalance:1500\tdiscount:NONE\n";

        System.out.println("Testing TollRoadMain class...");
        System.out.println("\tTesting TollRoadMainInitialise...");
        String customerDataStr;
        try // Read in our customers
        {
            customerDataStr = new String(Files.readAllBytes(
                    Paths.get("customerDataTest.txt")));
        }
        catch (IOException e) // Abort if we cant read the file
        {
            System.out.println("Could not read customerDataTest.txt!");
            return false;
        }
        // Check it's what we expected
        if(!customerDataStr.equals(expectedPreSplit))
        {
            System.out.println("Customer data is not what was expected!");
            return false;
        }

        // Otherwise, we read it fine. So lets split each customer out.
        // Then remove _all_ the control characters, because i'm
        // too lazy to deal with window's weird ass control characters.
        String[] customers = customerDataStr.split("#");
        for(int i = 0; i < customers.length; ++i)
        {
            customers[i] = customers[i].replaceAll("[\\p{Cntrl}]","");
        }
        if (customers.length != expectedPostSplit.length)
        {
            System.out.println("Size of split arrays dont match!");
            return false;
        }

        // Apparently deepEquals works, but x.equals(y) does not.
        if(Arrays.deepEquals(customers,expectedPostSplit) == false)
        {
            System.out.println("Arrays after split on # dont match!");
            return false;
        }

        // Make an array with the size of the amount we expect.
        CustomerAccount[] accounts = new CustomerAccount[
                expectedPostSplit.length];

        // Now tokenize our array of customers into tokens.
        for(int i = 0; i < customers.length; ++i)
        {
            String[] tokens = customers[i].split(",");
            // Check our tokenization is what we expected
            if(!Arrays.deepEquals(tokens, expectedPostTokenize[i]))
            {
                System.out.println("Tokens do not match expected at" +
                        " iteration " + i);
                return false;
            }

            // Make a vehicle switching based on the type we read
            Vehicle customerv;
            switch(tokens[0]){
                case "Car" : customerv = new Car(
                        tokens[1], tokens[4], Integer.parseInt(tokens[6]));
                    break;

                case "Truck" : customerv = new Truck(
                        tokens[1], tokens[4], Integer.parseInt(tokens[6]));
                    break;

                case "Van" : customerv = new Van(
                        tokens[1], tokens[4], Integer.parseInt(tokens[6]));
                    break;

                default: System.out.println("No vehicle in tokens!");
                    return false;
            }
            // Make a new account with what we tokenized, and append that to the array
            CustomerAccount newacc = new CustomerAccount(
                   tokens[2], tokens[3], customerv,Integer.parseInt(tokens[6]));
            accounts[i] = newacc;
        }
        // For each customer, add them to the map
        for(CustomerAccount acc : accounts)
        {
            tollroad.addCustomer(acc);
        }
        // Check it's what we expected
        if(finalExpected.equals(tollroad.toString()))
        {
            // If we get here, we're good!
            return true;
        }
        else
        {
            // Something's wrong...
            System.out.println("Final expected strings did not match!");
            return false;
        }
    }

    boolean testSimulate() throws
            CustomerNotFoundException, InsufficientAccountBalanceException
    {
        // Yes, a tokenizer class would have been "smart".
        // But turning all the parsing and tokenising into a class is more
        // effort than it's actually worth.
        System.out.println("\tTesting TollRoadMainSimulate...");
        String operationstr;
        try
        {
            operationstr = new String(Files.readAllBytes(Paths.get
                    ("testTransactions.txt")));
        } catch (IOException e)
        {
            System.out.println("Could not read testTransactions.txt");
            return false;
        }

        // Split into lines.
        String[] lines = operationstr.split("\\$");
        // Remove control chars
        for(int i =0; i < lines.length; ++i)
        {
            lines[i] = lines[i].replaceAll("[\\p{Cntrl}]","");
        }

        // Tokenize each line.
        for(int i = 0; i < lines.length; ++i)
        {
            // Split our opcode and operand(s)
            String[] ops = lines[i].split(",");

            // Java lambdas are kinda grim. Having to declare a interface for
            // each variant of a lambda isn't the coziest thing.
            // They're impure becuase they side effect which irks me, but w/e
            addFunds fundsLamb = ((regno, val) ->
                tollroad.findCustomer(regno).addFunds(Integer.parseInt(val)));
            makeTrip tripLamb = (regno ->
                tollroad.chargeCustomer(regno));

            // Switch on op
            switch(ops[0]){
                case "addFunds": fundsLamb.run(ops[1], ops[2]); break;
                case "makeTrip": tripLamb.run(ops[1]); break;
            }
        }
        if(tollroad.findCustomer("CB13IIZ").getAccountBalance() != 1000)
        {
            System.out.println("Balance expected to be 1000\nGot" +
                    tollroad.findCustomer("CB13IIZ")
                            .getAccountBalance());
            return false;
        }
        return true;
    }

    interface addFunds{
        void run(String regno, String val) throws CustomerNotFoundException;
    }
    interface makeTrip{
        void run(String regno) throws
                CustomerNotFoundException, InsufficientAccountBalanceException;
    }
}
