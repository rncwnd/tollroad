package test;
import tollroad.Van;

public class VanTest
{
    boolean test()
    {
        String expected = "Type:Van\tregNo:TESTPLATE\tMake:TESTMAKE\t" +
                "Payload:650\tTripCost:750";
        Van testvan = new Van("TESTPLATE", "TESTMAKE", 650);
        System.out.println("Testing Van class...");
        String result = testvan.toString();

        if(expected.equals(result))
            return true;
        else
        {
            System.out.println("\tExpected:\n\t\t" + expected);
            System.out.println("\tGot:\n\t\t" + result);
            return false;
        }
    }
}
