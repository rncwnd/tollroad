package test;
import tollroad.*;

public class TollRoadTest
{
    boolean test()
    {
        System.out.println("Testing TollRoad class...");

        TollRoad testtollroad = new TollRoad();
        Car testvehicle = new Car("13-954",
                "Toyota Sprinter Trueno AE86", 4);
        CustomerAccount testacc = new CustomerAccount("Takumi",
                "Fujiwara", testvehicle, 500);
        testtollroad.addCustomer(testacc);

        try
        {
            // Try and find the account we just created and added.
            CustomerAccount found = testtollroad.findCustomer("13-954");
            Car customercar = (Car)found.getCustomerVehicle();
            try
            {
                // Get the car from the account and then try charge the customer.
                testtollroad.chargeCustomer(customercar.getRegNo());
            }
            catch (InsufficientAccountBalanceException e)
            {
                // If they somehow don't have enough money, print a useful
                // error message and then a java stacktrace.
                e.printStackTrace();
                System.out.println("\tTest customer should always" +
                        " have enough balance" +
                        "to make the trip. Something is very wrong!");
                return false;
            }
            // If they both succeded, we're good to go and we pass the test
            return true;
        }
        catch (CustomerNotFoundException e)
        {
            // Catch in the case we somehow dont find the customer.
            // Print a useful error, and then a stack trace.
            System.out.println("\tCustomer with plate 13-954" +
                    " could not be found!" +
                    "Something is very wrong!");
            e.printStackTrace();
            return false;
        }
    }
}
