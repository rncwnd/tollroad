package test;
import tollroad.Car;

class CarTest
{
    /**
     * Tests are handled by creating expected strings and comparing the output
     * of the functions to them. If they dont match, return false
     * @return error code
     */
    boolean test()
    {
        String expected = "Type:Car\tregNo:TESTPLATE\tMake:TESTMAKE" +
                "\tNoSeats:4\tTripCost:500";

        Car testcar = new Car("TESTPLATE", "TESTMAKE",
                4);

        System.out.println("Testing Car class...");
        String result = testcar.toString();

        if(expected.equals(result))
            return true;
        else
        {
            System.out.println("\tExpected:\n\t\t" + expected);
            System.out.println("\tGot:\n\t\t" + result);
            return false;
        }
    }
}
