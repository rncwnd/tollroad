package test;
import tollroad.Truck;

public class TruckTest
{
    boolean test()
    {
        String expected = "Type:Truck\tregNo:TESTPLATE\t" +
                "Make:TESTMAKE\ttrailerNo:3\tTripCost:1500";
        Truck testtruck = new Truck("TESTPLATE", "TESTMAKE", 3);
        System.out.println("Testing Truck class...");
        String result = testtruck.toString();

        if(expected.equals(result))
            return true;
        else
        {
            System.out.println("\tExpected:\n\t\t" + expected);
            System.out.println("\tGot:\n\t\t" + result);
            return false;
        }
    }
}
