package test;

/**
 * Janky homebrewed testing harness.
 * If anything fails it throws an exception and aborts on the user.
 */
public class TestMain
{
    public static void main(String[] args) throws Exception
    {
        CarTest cartest = new CarTest();
        if(!cartest.test()){
            throw new Exception("CarTest Failed");
        }
        else{
            System.out.println("\tPass!");
        }
        VanTest vantest = new VanTest();
        if(!vantest.test()){
            throw new Exception("VanTest Failed!");
        }
        else{
            System.out.println("\tPass!");
        }
        TruckTest trucktest = new TruckTest();
        if(!trucktest.test()){
            throw new Exception("TruckTest Failed!");
        }
        else{
            System.out.println("\tPass!");
        }
        CustomerAccountTest acctest = new CustomerAccountTest();
        if(!acctest.test()){
            throw new Exception("CustomerAccountTest Failed!");
        }
        else{
            System.out.println("\tPass!");
        }
        TollRoadTest tolltest = new TollRoadTest();
        if(!tolltest.test()){
            throw new Exception("TollRoadTest Failed!");
        }
        else{
            System.out.println("\tPass!");
        }
        TollRoadMainTest trmt = new TollRoadMainTest();
        if(!trmt.testInitialise()){
            throw new Exception("TollRoadMainTestInitialise Failed!");
        }
        else{
            System.out.println("\t\tPass!");
        }
        if(!trmt.testSimulate()){
            throw new Exception("TollRoadMainTestSimulate Failed!");
        }
        else{
            System.out.println("\t\tPass!");
        }
    }
}
